const express = require('express')
const app = express()
const { port } = require('./config');
const path = require('path');
const logFilePath = path.join(__dirname, './urlLogFile.log');
const requestId = require('express-request-id');
const homePage = path.join(__dirname, './index.html');
const fs = require('fs');

app.use(requestId());

function urlLog(request, response, next) {
    const requestedURL = request.url;
    let data = `Request Id : ${request.id}, Method Type : ${request.method}, request url : ${requestedURL}`
    fs.appendFile(logFilePath, `${data}  ${new Date()} \n \n`, (error, data) => {
        if (error) {
            next({
                message : error,
                status : 400
            })
        }
    });
    next();
}

app.use(urlLog);

app.get('/', (request, response) => {
    response.sendFile(homePage);
    //response.send('Hello ')
})

app.get('/log', (request, response) => {
    response.sendFile(logFilePath);
})

app.use('/matchesperyear', require('./src/server/1-matches-per-year.cjs'))
app.use('/matches-won-per-team-per-year', require('./src/server/2-matches-won-per-team-per-year.cjs'));
app.use('/extra-run-per-team-2016', require('./src/server/3-extra-run-per-team-2016.cjs'));
app.use('/top-10-economical-bowler', require('./src/server/4-top-10-economical-bowler-2015.cjs'));
app.use('/number-of-times-each-team-won-toss-and-also-won-matches', require('./src/server/5-number-of-times-each-team-won-toss-and-also-won-matches.cjs'));
app.use('/player-with-highest-PlayerOfMatchAward', require('./src/server/6-player-with-highest-PlayerOfMatchAward.cjs'));
app.use('/strikeRate-of-batsman-each-season', require('./src/server/7-strikeRate-of-batsman-each-season.cjs'));
app.use('/highest-number-of-times-one-player-dismissed-by-another-player', require('./src/server/8-highest-number-of-times-one-player-dismissed-by-another-player.cjs'));
app.use('/bowler-with-best-economy-in-superover', require('./src/server/9-bowler-with-best-economy-in-superover.cjs'));


app.use((error, request, response, next) => {
    response.send({
        message: error.message,
        status: error.status
    })
})

app.use((request, response, next) => {
    response.status(404).send({
        message: "Invalid URL"
    })
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})