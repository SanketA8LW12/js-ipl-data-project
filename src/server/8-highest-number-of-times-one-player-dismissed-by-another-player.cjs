const express = require('express')
const appEight = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/8-highest-number-of-times-one-player-dismissed-by-another-player.json')

appEight.get('/', async (request, response) => {
    try {
        const deliveriesArray = await csvtojson().fromFile(csvDeleveriesPathFile);

        let bowlerDismissals = deliveriesArray.reduce((acc, current) => {

            if (current.dismissal_kind !== 'run out' && current.dismissal_kind !== '' && current.dismissal_kind !== 'hit wicket' && current.dismissal_kind !== 'retired hurt') {
                if (current.bowler in acc) {
                    if (acc[current.bowler][current.player_dismissed]) {
                        acc[current.bowler][current.player_dismissed] += 1
                    }
                    else {
                        acc[current.bowler][current.player_dismissed] = {}

                        acc[current.bowler][current.player_dismissed] = 1
                    }
                }

                else {
                    acc[current.bowler] = {}
                    acc[current.bowler][current.player_dismissed] = 1
                    //acc[current.bowler][current.player_dismissed]['dismissals'] = 1
                }
            }

            return acc;

        }, {});

        //console.log(bowlerDismissals);
        let result = Object.entries(bowlerDismissals)
            .map(bowler => {
                let batsman = Object.entries(bowler[1]).sort((a, b) => {
                    return b[1] - a[1];
                })
                return [bowler[0], batsman[0]];
            }).sort((a, b) => {

                return b[1][1] - a[1][1];
            });
        result = result[0]

        response.json(result);
    } catch (error) {
        next({
            message: "Internal Server Error",
            status: 500
        });
    }


});

module.exports = appEight;






