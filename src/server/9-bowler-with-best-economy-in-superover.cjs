const express = require('express')
const appNine = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
//const outputFilePath = path.join(__dirname, './../public/output/9-bowler-with-best-economy-in-superover.json')

appNine.get('/', async (request, response) => {
    try{
    const deliveriesArray = await csvtojson().fromFile(csvDeleveriesPathFile);
    // console.log(data);
    const bowlerWithBestEconomyInSuperOver = Object.entries(deliveriesArray.filter((item) => {
        return item.is_super_over !== "0";
    }).reduce((accumulator, match) => {
        if (match.bowler in accumulator) {
            accumulator[match.bowler]['runs'] += Number(match.total_runs);
            accumulator[match.bowler]['balls'] += 1;
        } else {
            accumulator[match.bowler] = {};
            accumulator[match.bowler]['runs'] = Number(match.total_runs);
            accumulator[match.bowler]['balls'] = 1;
        }
        return accumulator;
    }, {})).sort((bowlerA, bowlerB) => {
        const economyA = bowlerA[1].runs / bowlerA[1].balls;
        const economyB = bowlerB[1].runs / bowlerB[1].balls;
        return economyA - economyB;
    }).slice(0, 1);

    response.json(bowlerWithBestEconomyInSuperOver);
    }catch(error){
        next({
            message : "Internal Server Error",
            status : 500
        });
    }
    

})

module.exports = appNine;
