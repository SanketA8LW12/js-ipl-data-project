const express = require('express')
const appFive = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/5-number-of-times-each-team-won-toss-and-also-won-matches.json')

appFive.get('/', async (request, response) => {

  try {
    const matchesArray = await csvtojson().fromFile(csvPathFile);

    let teamWonToss = matchesArray.reduce((acc, current) => {
      if (current.toss_winner === current.winner) {
        if (acc[current.toss_winner]) {
          acc[current.toss_winner] += 1;
        }
        else {
          acc[current.toss_winner] = 1;
        }
      }
      return acc;
    }, {});

    response.json(teamWonToss);
  } catch (error) {
    next({
      message: "Internal Server Error",
      status: 500
    });
  }


})

module.exports = appFive;