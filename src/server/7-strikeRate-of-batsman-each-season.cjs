const express = require('express')
const appSeven = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const { default: appEight } = require('./8-highest-number-of-times-one-player-dismissed-by-another-player.cjs');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/7-strikerate-of-batsman-each-season.json')



appSeven.get('/', async (request, response) => {
    try {
        const matcheArray = await csvtojson().fromFile(csvPathFile);
        const deliveriesArray = await csvtojson().fromFile(csvDeleveriesPathFile);
        let seasonsAndMatchIds = matcheArray.reduce((result, match) => {

            result[match.id] = match.season

            return result
        }, {})


        let batsmanStrikeRates = deliveriesArray.map((delivery) => {

            delivery.match_id = seasonsAndMatchIds[delivery.match_id]
            return delivery
        }).reduce((acc, current) => {
            let ball = 1
            let totalBalls = 0
            let totalRuns = 0
            if (current.wide_runs !== '0') {

                ball = 0
            }
            if (acc[current.batsman]) {
                if (acc[current.batsman][current.match_id]) {
                    acc[current.batsman][current.match_id]['runs'] += +current.batsman_runs
                    acc[current.batsman][current.match_id]['balls'] += ball
                }
                else {
                    acc[current.batsman][current.match_id] = {}
                    acc[current.batsman][current.match_id]['runs'] = +current.batsman_runs
                    acc[current.batsman][current.match_id]['balls'] = ball
                }
            } else {
                acc[current.batsman] = {}

                acc[current.batsman][current.match_id] = {}
                acc[current.batsman][current.match_id]['runs'] = +current.batsman_runs
                acc[current.batsman][current.match_id]['balls'] = ball
            }
            totalBalls = acc[current.batsman][current.match_id]['balls']
            totalRuns = acc[current.batsman][current.match_id]['runs']

            acc[current.batsman][current.match_id].StrickeRate = ((totalRuns / totalBalls) * 100).toFixed(2)
            return acc
        }, {})
        response.json(batsmanStrikeRates)
    } catch (error) {
        next({
            message: "Internal Server Error",
            status: 500
        });
    }

});

module.exports = appSeven;









