const express = require('express')
const appOne = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
let csvPathFile = path.resolve('src', 'data', 'matches.csv');
//const outputFilePath = path.join(__dirname, './../public/output/1-matches-per-year.json')
//console.log('csv path:', csvPathFile)

appOne.get('/', async (request, response) => {
    try{
    const matchArray = await csvtojson().fromFile(csvPathFile);

    let yearOfIpl = matchArray.reduce((acc, current) => {
        let iPLYear = current.season;
        if (iPLYear in acc) {
            acc[iPLYear] += 1
        }
        else {
            acc[iPLYear] = 1;
        }
        return acc;
    }, {})
    response.json(yearOfIpl);
}catch(error){
    next({
        message : "Internal Server Error",
        status : 500
    });
}
    //console.log(yearOfIpl);
    //fs.writeFileSync(outputFilePath, JSON.stringify(yearOfIpl, null, 2), 'utf8');

})

module.exports = appOne;










// async function matchesPerYear() {
//     const matchArray = await csvtojson().fromFile(csvPathFile);

//     let yearOfIPLObj = {};

//     for (let index = 0; index < matchArray.length; index++) {
//         const year = matchArray[index].season;
//         if (year in yearOfIPLObj) {
//             yearOfIPLObj[year] += 1;
//         }
//         else {
//             yearOfIPLObj[year] = 1;
//         }
//     } return yearOfIPLObj;
// }
// matchesPerYear().then(data => {

//     const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation

//     fs.writeFile('../public/output/1-matches-per-year.json', jsonData, 'utf8', (err) => {
//         if (err) {
//             console.error('Error writing JSON file:', err);
//         } else {
//             console.log('JSON file has been saved!');
//         }
//     });

// });