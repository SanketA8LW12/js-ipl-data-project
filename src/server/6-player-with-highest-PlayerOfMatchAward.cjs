const express = require('express')
const appSix = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/6-player-with-highest-PlayerOfMatchAward.json')


appSix.get('/', async (request, response) => {
  try {
    const matcheArray = await csvtojson().fromFile(csvPathFile);

    let yearOfIPL = matcheArray.reduce((acc, current) => {
      const year = current.season;
      const playerOfMatch = current.player_of_match;

      if (!(year in acc)) {
        acc[year] = {};
      }

      if (!(playerOfMatch in acc[year])) {
        acc[year][playerOfMatch] = 0;
      }

      acc[year][playerOfMatch]++;

      return acc;

    }, {});

    //console.log(yearOfIPL);

    let highestPlayerOfMatch = Object.entries(yearOfIPL).reduce((acc, current) => {
      const object = Object.entries(current[1])
        .sort((playerOne, playerTwo) => {
          return playerTwo[1] - playerOne[1];
        }).slice(0, 1);

      acc[current[0]] = Object.fromEntries(object);

      return acc;


    }, {});
    response.json(highestPlayerOfMatch);
  } catch (error) {
    next({
      message: "Internal Server Error",
      status: 500
    });
  }

})

module.exports = appSix;











