const express = require('express')
const appTwo = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
let csvPathFile = path.resolve('src','data','matches.csv');
const outputFilePath = path.join(__dirname, './../public/output/2-matches-won-per-team-per-year.json')


appTwo.get('/', async (request, response) => {
  try {
  const matchArray = await csvtojson().fromFile(csvPathFile);

  let yearOfIPLObj = matchArray.reduce((acc, current) => {
    const year = current.season;
    const matchWinner = current.winner;
    if(year in acc){
      if(matchWinner in acc[year]){
        acc[year][matchWinner] += 1
      }
      else{
        acc[year][matchWinner] = 1
      }
    }
    else{
      acc[year] = {};
    }

    return acc;
  }, {})
  
  response.json(yearOfIPLObj);
}catch(error){
  next({
      message : "Internal Server Error",
      status : 500
  });
}

});



module.exports = appTwo;
  











// async function matchesWonPerTeam(){
//   const matchArray = await csvtojson().fromFile(csvPathFile);
  
//   let yearOfIPLObj = {};
  
//   for(let index = 0; index < matchArray.length; index++){
//       const year = matchArray[index].season;
//       const matchWinner = matchArray[index].winner;
  
//       if(year in yearOfIPLObj){
//           if(matchWinner in yearOfIPLObj[year]){
//               yearOfIPLObj[year][matchWinner] += 1
//           }
//           else{   
//               yearOfIPLObj[year][matchWinner] = 1
//           }
             
//       }
//       else{
//           yearOfIPLObj[year] = {};
  
//       }
//   }
  
//   return yearOfIPLObj;
  
//   }
//   matchesWonPerTeam().then(data=>{
  
//       const jsonData = JSON.stringify(data, null, 2);   // Convert object to JSON string with indentation
  
//   fs.writeFile('../public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf8',(err) => {
//     if (err) {
//       console.error('Error writing JSON file:', err);
//     } else {
//       console.log('JSON file has been saved!');
//     }
//   });
  
//   });