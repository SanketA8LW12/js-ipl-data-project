const express = require('express')
const appThree = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/3-extra-run-per-team-2016.json')



appThree.get('/', async (request, response) => {
    try {
        const matchArray = await csvtojson().fromFile(csvPathFile);
        const deliveryArray = await csvtojson().fromFile(csvDeleveriesPathFile);

        let matchIdArray = matchArray.filter((match) => {
            return match.season === '2016';
        }).map((match) => {
            return match.id;
        })

        let extraRunObject = deliveryArray.reduce((acc, current) => {
            if (matchIdArray.includes(current.match_id)) {

                let team = current.bowling_team;
                if (team in acc) {
                    acc[team] += Number(current.extra_runs);
                }
                else {
                    acc[team] = Number(current.extra_runs);
                }
            }
            return acc;
        }, {});
        response.json(extraRunObject);
    } catch (error) {
        next({
            message: "Internal Server Error",
            status: 500
        });
    }

})

module.exports = appThree;