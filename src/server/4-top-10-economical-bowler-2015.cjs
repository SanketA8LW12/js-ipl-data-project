const express = require('express')
const appFour = express.Router();
const { port } = require('./../../config');
const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const csvPathFile = path.resolve('src', 'data', 'matches.csv');
const csvDeleveriesPathFile = path.resolve('src', 'data', 'deliveries.csv');
const outputFilePath = path.join(__dirname, './../public/output/4-top-10-economical-bowler-2015.json')

appFour.get('/', async (request, response) => {
    try {
        const matches = await csvtojson().fromFile(csvPathFile);
        const deliveries = await csvtojson().fromFile(csvDeleveriesPathFile);

        let matchIdArray = matches.filter((match) => {
            return match.season === '2015';
        }).map((match) => {
            return match.id;
        })

        //console.log(matchIdArray);

        let bowlerRecord = deliveries.reduce((acc, current) => {

            if (matchIdArray.includes(current.match_id)) {
                let ball = Number(current.ball);
                let run = Number(current.total_runs);
                let bowler = current.bowler;
                let bowlerInfo = {};
                if (bowler in acc) {
                    bowlerInfo = acc[bowler];
                    bowlerInfo.run += run;
                    bowlerInfo.ball += 1;
                }
                else {
                    bowlerInfo['ball'] = ball;
                    bowlerInfo['run'] = run;
                    acc[bowler] = bowlerInfo;
                }
            }
            return acc;

        }, {});

        //console.log(bowlerRecord);

        let bowlerObject = Object.entries(bowlerRecord);

        let bowlerWithEconomy = bowlerObject.reduce((acc, current) => {
            //console.log(current);
            let ball = current[1].ball;
            // console.log(ball);
            let run = current[1].run;
            //console.log(run);
            let economy = (run / ball) * 6;
            acc[current[0]] = economy;
            return acc;
        }, {});


        let bowlerEconomyArr = Object.entries(bowlerWithEconomy);
        bowlerEconomyArr.sort((bowlerOne, bowlerTwo) => {
            return bowlerOne[1] - bowlerTwo[1];
        });

        response.json(bowlerEconomyArr.slice(0, 10));
    } catch (error) {
        next({
            message: "Internal Server Error",
            status: 500
        });
    }

});

module.exports = appFour;